/* ******************************************************************
 * Constantes de configuration
 */
const apiKey = "fb07b2be-43ef-4c92-bf5a-3a39b1fda413";
const serverUrl = "https://lifap5.univ-lyon1.fr";

/* ******************************************************************
 * Gestion des tabs "Voter" et "Toutes les citations"
 ******************************************************************** */

/**
 * Affiche/masque les divs "div-duel" et "div-tout"
 * selon le tab indiqué dans l'état courant.
 *
 * @param {Etat} etatCourant l'état courant
 * @brief Fonction d'origine et appel à 2 nouvelles fonctions.
 * @brief Récupération élément du duel de manière globale ou spécifique.
 * @brief Appel au fonction de raffraichisement pour le tableau aprés un vote. 
 */
function majTab(etatCourant) {
  console.log("CALL majTab");
  if (!etatCourant.login) {
    document.getElementById('btn-open-login-modal').innerHTML = "Connexion";
  } else {
    document.getElementById('btn-open-login-modal').innerHTML = "Deconnexion";
  }
  const dDuel = document.getElementById("div-duel");
  const dTout = document.getElementById("div-tout");
  const tDuel = document.getElementById("tab-duel");
  const tTout = document.getElementById("tab-tout");
  if (etatCourant.tab === "duel") {
    dDuel.style.display = "flex";
    tDuel.classList.add("is-active");
    dTout.style.display = "none";
    tTout.classList.remove("is-active");
    charge_donnees('citations', 'duel', etatCourant);
    document.getElementById("voteD").onclick=function(){ vote("droite",etatCourant)}
    document.getElementById("voteG").onclick=function(){ vote("gauche",etatCourant)}
  } else {
    charge_donnees('citations', 'citations', etatCourant);
    document.getElementById("VerifFormAdd").onclick = function () { verif_formAdd(etatCourant) }
    dTout.style.display = "flex";
    tTout.classList.add("is-active");
    dDuel.style.display = "none";
    tDuel.classList.remove("is-active");
  }
}

/**
 * Mets au besoin à jour l'état courant lors d'un click sur un tab.
 * En cas de mise à jour, déclenche une mise à jour de la page.
 *
 * @param {String} tab le nom du tab qui a été cliqué
 * @param {Etat} etatCourant l'état courant
 */
function clickTab(tab, etatCourant) {
  console.log(`CALL clickTab(${tab},...)`);
  if (etatCourant.tab !== tab) {
    etatCourant.tab = tab;
    majPage(etatCourant);
  }
}

/**
 * Enregistre les fonctions à utiliser lorsque l'on clique
 * sur un des tabs.
 *
 * @param {Etat} etatCourant l'état courant
 */
function registerTabClick(etatCourant) {
  console.log("CALL registerTabClick");
  document.getElementById("tab-duel").onclick = () =>
    clickTab("duel", etatCourant);
  document.getElementById("tab-tout").onclick = () =>
    clickTab("tout", etatCourant);
}

/* ******************************************************************
 * Gestion de la boîte de dialogue (a.k.a. modal) d'affichage de
 * l'utilisateur.
 * ****************************************************************** */

/**
 * Fait une requête GET authentifiée sur /whoami
 * @returns une promesse du login utilisateur ou du message d'erreur
 */
function fetchWhoami(etatCourant) {
  return fetch(serverUrl + "/whoami", { 
    headers: { "x-api-key": etatCourant.api } })
    .then((response) => response.json())
    .then((jsonData) => {
      if (jsonData.status && Number(jsonData.status) != 200) {
        return { err: jsonData.message };
      }
      return jsonData;
    })
    .catch((erreur) => ({ err: erreur }));
}

/**
 * Fait une requête sur le serveur et insère le login dans
 * la modale d'affichage de l'utilisateur.
 *
 * @param {Etat} etatCourant l'état courant
 * @returns Une promesse de mise à jour
 */
function lanceWhoamiEtInsereLogin(etatCourant) {
  return fetchWhoami(etatCourant).then((data) => {
    etatCourant.login = data.login; // qui vaut undefined en cas d'erreur
    etatCourant.errLogin = data.err; // qui vaut undefined si tout va bien
    if (data.err) {
      document.getElementsByClassName("popup")[0].className = "popup card show";
      setTimeout(function () {
      document.getElementsByClassName("popup")[0].className =
      document.getElementsByClassName("popup")[0].className.replace("show","");
      }, 3000);
    }
    majPage(etatCourant);
    // Une promesse doit renvoyer une valeur, mais celle-ci n'est pas importante
    // ici car la valeur de cette promesse n'est pas utilisée. On renvoie
    // arbitrairement true
    return true;
  });
}


/**
 * Affiche ou masque la fenêtre modale de login en fonction de l'état courant.
 * Change la valeur du texte affiché en fonction de l'état
 *
 * @param {Etat} etatCourant l'état courant
 */
function majModalLogin(etatCourant) {
  const modalClasses = document.getElementById("mdl-login").classList;
  if (etatCourant.loginModal) {
    modalClasses.add("is-active");
    const elt = document.getElementById("elt-affichage-login");
    const ok = etatCourant.login !== undefined;
    if (!ok) {
      elt.innerHTML = "<input type='password' id='api' class='input'>" +
        `<button id='connexion' >Connexion</button>`;
      document.getElementById("connexion").
        addEventListener('click', function handler(etatCourant) {
          etatCourant.api = document.getElementById('api').value;
          document.getElementById("connexion").
            removeEventListener('click', handler);
          lanceWhoamiEtInsereLogin(etatCourant);
        })
    } else { elt.innerHTML = `Bonjour ${etatCourant.login}.`; }
  } else { modalClasses.remove("is-active"); }
}

/**
 * Déclenche l'affichage de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickFermeModalLogin(etatCourant) {
  etatCourant.loginModal = false;
  majPage(etatCourant);
}

/**
 * Déclenche la fermeture de la boîte de dialogue du nom de l'utilisateur.
 * @param {Etat} etatCourant
 */
function clickOuvreModalLogin(etatCourant) {
  etatCourant.loginModal = true;

  //lanceWhoamiEtInsereLogin(etatCourant);
  majPage(etatCourant);
}

/**
 * Enregistre les actions à effectuer lors d'un click sur les boutons
 * d'ouverture/fermeture de la boîte de dialogue affichant l'utilisateur.
 * @param {Etat} etatCourant
 */
function registerLoginModalClick(etatCourant) {
  document.getElementById("btn-close-login-modal1").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-close-login-modal2").onclick = () =>
    clickFermeModalLogin(etatCourant);
  document.getElementById("btn-open-login-modal").onclick = () =>
    clickOuvreModalLogin(etatCourant);
}

/* ******************************************************************
 * Initialisation de la page et fonction de mise à jour
 * globale de la page.
 * ****************************************************************** */

/**
 * Mets à jour la page (contenu et événements) en fonction d'un nouvel état.
 *
 * @param {Etat} etatCourant l'état courant
 */
function majPage(etatCourant) {
  console.log("CALL majPage");
  majTab(etatCourant);
  majModalLogin(etatCourant);
  registerTabClick(etatCourant);
  registerLoginModalClick(etatCourant);
}

/**
 * Appelé après le chargement de la page.
 * Met en place la mécanique de gestion des événements
 * en lançant la mise à jour de la page à partir d'un état initial.
 */
function initClientCitations() {
  console.log("CALL initClientCitations");
  const etatInitial = {
    api: undefined,
    tab: "duel",
    loginModal: false,
    login: undefined,
    errLogin: undefined
  };
  majPage(etatInitial);
}

// Appel de la fonction init_client_duels au après chargement de la page
document.addEventListener("DOMContentLoaded", () => {
  console.log("Exécution du code après chargement de la page");
  initClientCitations();
});

/** 
 *@brief Affiche les citations dans l'onglet "Toutes les citations".
 *
 *@param {Number} classement le classement de la citation 
 *@param {String} personnage le nom du personnage 
 *@param {String} citation la citation 
 *@param {String} id l'id de la citation 
 *@param {String} addedBy l'auteur de la citation
 *@param {Etat} etatCourant l'etat courant   
 */
function aff_toute_cita(classement, personnage, citation, id, addedBy, etatCourant) {
  if (addedBy == undefined) {
    addedBy = "";
  }
  let don = acces_modifi(addedBy, etatCourant, id);
  document.getElementById("tout").innerHTML += "<tr><th>" + classement +
    "</th><td>" + personnage +
    `</td><td onclick="detailCitation('` + id + `')">` + citation +
     "</td>" + "<td>" + don + "</td>" + "</tr>";
  let buttons = document.querySelectorAll(".Modif");
  let tab_button = Array.from(buttons);
  tab_button.map((button) => {
    button.onclick= ()=>{ modal_modifier_citation(button.value, etatCourant); }
  })
}

/**
 *@brief Permet de mettre en forme les citations.
 *@param {Object} arr tableau d'objet.
 *@param {Etat} etatCourant l'état courant. 
 */
function format_citations(arr, etatCourant) {
  document.getElementById("tout").innerHTML = ""; // Évite duplication.
  arr.reduce((acc, x) => {
    aff_toute_cita(1, x.character, x.quote, x._id, x.addedBy, etatCourant);
  });
}

/**
 *@brief Charge l'un des modes sur la page : duel ou toutes les ciations.
 *@param {String} url L'url de la citation
 *@param {String} use Récupere les citations de la BD, sinon 2 pour le duel.
 *@param {Etat} etatCourant l'état courant 
 */
function charge_donnees(url, use, etatCourant) {

  fetch(serverUrl + "/" + url, { headers: { "x-api-key": etatCourant.api } })
    .then(res => res.json())
    .then(data => obj = data)
    .then(() => {
      if (use == 'citations') { format_citations(obj, etatCourant) }
      if (use == 'duel') { duel_rand(obj) }
    })
}

/**
 *@brief Dans la page duel :
 *@brief Affiche deux citations aléatoires via fonction elemAleatoire.
 *@param {Object} data données relatifs aux citations.   
 * 
 */
function duel_rand(data) {
  let temp = data;
  let elem1 = elemAleatoire(temp);
  let elem2 = elemAleatoire(temp);
  aff_rand(elem1, 0);
  aff_rand(elem2, 1);
}

/**
 *@brief Affiche les images durant le duel et les positionnent correctement.
 *@param {String} direction l'orientation de l'image.    
 *@param {String} lienImg l'url de l'image.
 *@param {Number} n indice de position sur la page.
 */
function add_new_image(direction, lienImg, n) {
  document.getElementsByClassName("image_card")[n]
    .src = lienImg;
  if (direction == "Left" && n == 0) {
    document.getElementsByClassName("image_card")[n]
      .style.transform = "rotateY(0deg)";
  } else if (direction == "Right" && n == 1) {
    document.getElementsByClassName("image_card")[n]
      .style.transform = "rotateY(0deg)";
  } else if (direction == "Left" && n == 1) {
    document.getElementsByClassName("image_card")[n]
      .style.transform = "rotateY(-160deg)";
  } else {
    document.getElementsByClassName("image_card")[n]
      .style.transform = "rotateY(-160deg)";
  }
}

/**
 *@brief affiche aléatoirement les citations.
 *@param {Object} data données relatifs aux citations. 
 *@param {Number} n indice de position sur la page.  
 * 
 */
function aff_rand(data, n) {
  document.getElementsByClassName("titre")[n]
    .innerHTML = '"' + data.quote + '"';
  document.getElementsByClassName("description")[n]
    .innerHTML = data.character + " dans " + data.origin;
  add_new_image(data.characterDirection, data.image, n);
  add_new_image(data.characterDirection, data.image, n);
  document.getElementsByClassName("id")[n]
    .innerHTML = data._id;
}

/**
 * @brief Prend un chiffre semi-aléatoire qui sera utiliser dans duel_rand
 * @param {Object} tab tableau de l'ensemble des citations.
 * @returns Une ligne (objet) issu du tableau
 */
function elemAleatoire(tab) {
  let randomIndex = Math.floor(Math.random() * tab.length);
  return tab.splice(randomIndex, 1)[0];
}


/**
 *@brief Permet de voter pour l'une des deux citations
 *@param {String} elem chaine de caractère correspondant a l'élément gagnant.
 *@param {Etat} etatCourant l'état courant 
 */
function vote(elem, etatCourant) {
  let id_winner, id_looser;
  if (elem == "gauche") {
    id_winner = document.getElementsByClassName("id")[0].innerHTML;
    id_looser = document.getElementsByClassName("id")[1].innerHTML;
    alert("Vote à gauche");
  } else {
    id_winner = document.getElementsByClassName("id")[1].innerHTML;
    id_looser = document.getElementsByClassName("id")[0].innerHTML;
    alert("Vote à droite");
  }
  fetch(serverUrl + "/citations/duels", {
    method: 'POST', headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'x-api-key': etatCourant.api
    },
    body: JSON.stringify({ winner: id_winner, looser: id_looser })
  })
  charge_donnees('citations', 'duel', etatCourant);
}

/**
 * @brief Affiche les détails d'une citation se trouvant dans le tableau.
 * @param {String} id identifiant de la citation 
 */
function detailCitation(id) {
  fetch(serverUrl + "/citations/" + id, {
    method: 'GET',headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then(res => res.json()).then(data => obj = data).then( () => {
      document.getElementById("modal-detail").style.display = "block";
      document.getElementById("quote").innerHTML =
        '<span class="gras">Citation : "</span>' + obj.quote + '"';
      document.getElementById("character").innerHTML =
        '<span class="gras">Personnage :</span>' + obj.character;
      document.getElementById("image").innerHTML =
        "<span class='gras'>Url de l'image : </span><a>" + obj.image + "</a>";
      document.getElementById("character-direction").innerHTML =
        "<span class='gras'>Direction personnage :</span>"+obj.characterDirection;
      document.getElementById("origin").innerHTML =
        "<span class='gras'>Origine : </span>" + obj.origin;
    })}

/**
 *@brief Ferme la modal de détail d'une citation
 */
function close_detail() {
  document.getElementById("modal-detail").style.display = "none";
}

/**
 *@brief Ouvre une modal qui permet l'ajout d'une citation
 */
function modal_add() {
  document.getElementById("modal-add").style.display = "block";
}

/**
 *@brief Ajoute une citation dans la BD
 *@param {Etat} etatCourant l'état courant 
 */
function add_citation(etatCourant) {
  let quote = document.getElementById("Quote").value;
  let personnage = document.getElementById("Perso").value;
  let origine = document.getElementById("Origine").value;
  let dir = document.getElementById("Direction").value;
  let url = document.getElementById("lien").value;
  fetch(serverUrl + "/citations", {
    method: 'POST',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      "x-api-key": etatCourant.api,
    },
    body: JSON.stringify({
      quote: quote, character: personnage,
      origin: origine, image: url, characterDirection: dir
    })
  })
}

/**
 *@brief Vérifies les champs obligatoire requis pour les nouvelles citations.
 *@param {Etat} etatCourant l'état courant 
 */
function verif_formAdd(etatCourant) {

  let quote = document.getElementById("Quote").value;
  let personnage = document.getElementById("Perso").value;
  let origine = document.getElementById("Origine").value;

  if (quote == "") {
    alert("Champs citation obligatoire");
  } else if (personnage == "") {
    alert("Champs personnage obligatoire")
  } else if (origine == "") {
    alert("Champs origine obligatoire")
  } else {
    alert("tout est correct")
    add_citation(etatCourant);
  }
}

/**
 *@brief Ferme la modal d'ajout d'une citation
 */
function close_add() {
  document.getElementById("modal-add").style.display = "none";
}

/**
 *@brief Affiche le boutton de modification pour l'auteur seulement.
 *@param {String} addedBy l'auteur de la citation.
 *@param {Etat} etatCourant l'état courant.
 *@param {String} id l'identifiant de la citation.
 */
function acces_modifi(addedBy, etatCourant, id) {
  if (addedBy == etatCourant.login) {
    return "<button class='icon is-large button has-text-success Modif' value='" + id + "'>"
      + "<i class='fas fa-edit fa-2x'></i></button>";
  } else {
    return '<button class="noedit icon button is-white" disabled>'+
      '<i class="fas fa-edit fa-2x"></i></button>';
  }
}

/**
 *@brief Modifie le formulaire de la citation par la methode PUT
 *@param {String} id  l'identifiant de la citation.
 * @param {Etat} etatCourant l'état courant 
 */
function requete_modifier(id, etatCourant) {
  const quote = document.getElementById("QuoteModif").value;
  const personnage = document.getElementById("PersoModif").value;
  const origine = document.getElementById("OrigineModif").value;
  const dir = document.getElementById("DirectionModif").value;
  const url = document.getElementById("lienModif").value;
  fetch(serverUrl + "/citations/" + id, {
    method: 'PUT',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      "x-api-key": etatCourant.api,
    },
    body: JSON.stringify({
      quote: quote, character: personnage,
      origin: origine, image: url, characterDirection: dir
    })
  })
}

/**
 *@brief Récupere la citation pour pouvoir la modifier avec la methode GET
 *@param {Number} id l'identifiant de la citation.
 */
function Recupere_citation(id) {
  fetch(serverUrl + "/citations/" + id, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
  })
    .then(res => res.json()).then(data => obj = data)
    .then(() => {
      document.getElementById("QuoteModif").value = obj.quote;
      document.getElementById("PersoModif").value = obj.character;
      document.getElementById("OrigineModif").value = obj.origin;
      document.getElementById("DirectionModif").value = obj.characterDirection;
      document.getElementById("lienModif").value = obj.url;
    })
}

/**
 *@brief Ouvre une modal pour l'auteur de la citation,
 *@brief qui récupère les données afin de les modifier.  
 *@param {String} id l'identifiant de la citation.  
 *@param {Etat} etatCourant l'état courant 
 */
function modal_modifier_citation(id, etatCourant) {
  document.getElementById("modal-modif").style.display = "block";
  Recupere_citation(id)
  document.getElementById("VerifFormModif").onclick = function () {
    verif_formModif(id, etatCourant)
  }
}

/**
 *@brief Ferme la modal relative au modification
 */
function close_modif() {
  document.getElementById("modal-modif").style.display = "none";
}

/**
 *@brief Vérifie le formulaire pour la modification
 *@param {String} id l'identifiant de la citation.  
 *@param {Etat} etatCourant l'état courant 
 */
function verif_formModif(id, etatCourant) {

  const quote = document.getElementById("QuoteModif").value;
  const personnage = document.getElementById("PersoModif").value;
  const origine = document.getElementById("OrigineModif").value;

  if (quote == "") {
    alert("Champs citation obligatoire");
  } else if (personnage == "") {
    alert("Champs personnage obligatoire")
  } else if (origine == "") {
    alert("Champs origine obligatoire")
  } else {
    alert("Modification effectuer")
    requete_modifier(id, etatCourant);
  }
}
